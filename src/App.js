// importo React a la aplicación
import React from 'react';
// importo componentes
import WeatherLocation from './components/WeatherLocation/';

// estilos de React
import './App.css';

function App() {
  return (
    <div className="App">
      <WeatherLocation></WeatherLocation>
    </div>
  );
}

export default App;
