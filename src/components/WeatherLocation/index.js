// importo React
// import React from 'react';
import React, { Component } from 'react';
import transformWeather from '../../services/transformWeather';
import { api_weather } from '../../constants/api_url'

// importo componentes
import Location from './Location';
import WeatherData from './WeatherData';
import './styles.css';

import{
    CLOUD,
    CLOUDY,
    SUN,
    RAIN,
    FOG,
    SNOW,
    WINDY,
} from '../../constants/weathers';

const data = {
    temperature: 18,
    weatherState: RAIN,
    humidity: 95,
    wind: '15 km/h'
}

// constante con el mismo nombre que tiene el archivo

// ## COMPONENTE FUNCIONAL
// const WeatherLocation = () => (
//     <div className="weatherLocationContent">
//         {/* A Location le establezco el parámetro 'city' con el valor 'Buenos Aires'.  */}
//         <Location city={'Buenos Aires'}></Location>
//         <WeatherData data={data}></WeatherData>
//     </div>
// );

// ## COMPONENTE DE CLASE
class WeatherLocation extends Component {

    // Para hacer que nuestro componente se renderice de acuerdo al state, utilizamos el 'constructor' del componente.
    constructor() {
        // invocamos al superconstructor
        super();
        
        // this.state, es el estado local/ parcial de nuestro componente. 
        this.state = {
            city: 'Buenos Aires',
            data: data,
        }
    }

    
    // Función del evento click
    handleUpdateClick = () => {
        fetch(api_weather).then( resolve => {
            return resolve.json();
        }).then( data => {
            const newWeather = transformWeather( data );
            console.log(newWeather);
            debugger;

            // con 'this.setState({}) modifico el estado inicial.
            this.setState({
                data: newWeather,
            })
        } )
        ;
    }
    render() {
        const { city, data } = this.state;
        return(
            <div className="weatherLocationContent">
                {/* A Location le establezco el parámetro 'city' con el valor 'Buenos Aires'.  */}
                <Location city={city}></Location>
                <WeatherData data={data}></WeatherData>

                {/* En el evento onclick hay que utilizar la palabra reservada 'this', para hacer referencia a la función. */}
                <button className="btn" onClick={this.handleUpdateClick}>Actualizar</button>
            </div>
        )
    }
};

// exporto mi componente
export default WeatherLocation;