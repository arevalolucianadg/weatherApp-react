import React from 'react';
import PropTypes from 'prop-types';

import './styles.css';

// el nombre de parámetro 'props' es un nombre estándar.
const Location = ({ city }) => (
    <div className="locationContent">
        {/* En HTML/JSX coloco el nombre de la constante dentro de llaves. */}
        <h1>{city}</h1>
    </div>

);

Location.propTypes = {
    // atajo: ptsr <tab>
    city: PropTypes.string.isRequired,
}

export default Location;