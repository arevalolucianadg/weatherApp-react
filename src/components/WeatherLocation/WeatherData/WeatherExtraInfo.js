import React from 'react';
import PropTypes from 'prop-types';

import './styles.css'

const WeatherExtraInfo = ({ humidity, wind }) => (
    <div className="weatherExtraInfoContent">
        <p className="weatherExtraInfoText">{`Humedad: ${humidity} %`}</p> 
        <p className="weatherExtraInfoText">{`Viento: ${wind}`}</p>
    </div>
);

WeatherExtraInfo.propTypes = {
    humidity: PropTypes.number.isRequired,
    wind: PropTypes.string.isRequired,
}

export default WeatherExtraInfo;