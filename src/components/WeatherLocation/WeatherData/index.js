import React from 'react';
import PropTypes from 'prop-types';

import WeatherExtraInfo from './WeatherExtraInfo';
import WeatherTemperature from './WeatherTemperature';

// importo las constantes creadas en constants/weathers.js
import {
    CLOUD,
    CLOUDY,
    SUN,
    RAIN,
    FOG,
    SNOW,
    WINDY,
} from '../../../constants/weathers';
// El uso de las constantes hace que el código sea más estructurado, y nos muestra errores en caso de que se escriba mal el nombre de la propiedad de un estado, en este caso.
// De esta forma, estamos estableciendo que valores serían los correctos.

import './styles.css'

// sintaxis version01
const WeatherData = ({ data: { temperature, weatherState, humidity, wind } }) => (
    // la etiqueta class en React, se escribe como 'className' al igual que sus propiedades.
    <div className="weatherDataContent">
        <WeatherTemperature
            temperature={temperature}
            weatherState={weatherState}
        />
        <WeatherExtraInfo humidity={humidity} wind={wind} />
    </div>
);

WeatherData.propTypes = {
    data: PropTypes.shape({
        temperature: PropTypes.number.isRequired,
        weatherState: PropTypes.string.isRequired,
        humidity: PropTypes.number.isRequired,
        wind: PropTypes.string.isRequired,
    }),
};

// sintaxis version02
// paso { data } como parámetro.
// const WeatherData = ({ data }) => {
//     // las 4 constantes se corresponden con las 4 propiedades de data.
//     const { temperature, weatherState, humidity, wind } = data;

//     return (// la etiqueta class en React, se escribe como 'className' al igual que sus propiedades.
//     <div className="weatherDataContent">
//         <WeatherTemperature
//             temperature={temperature}
//             weatherState={weatherState}
//         />
//         <WeatherExtraInfo humidity={humidity} wind={wind} />
//     </div>)
// };



export default WeatherData;