import React from 'react';
import WeatherIcons from 'react-weathericons';

// PROP TYPES: Herramienta que ayuda a validar los datos que le pasamos a los distintos componentes.
import PropTypes from 'prop-types';

import{
    CLOUD,
    CLOUDY,
    SUN,
    RAIN,
    FOG,
    SNOW,
    WINDY,
} from '../../../constants/weathers';

import './styles.css'

// diccionario de iconos, donde va a llegar el estado del clima y va a retornar el nombre del icono.
const icons = {
    [CLOUD]: 'cloud',
    [CLOUDY]: 'cloudy',
    [SUN]: 'day-sunny',
    [RAIN]: 'rain',
    [FOG]: 'day-fog',
    [SNOW]: 'snow',
    [WINDY]: 'windy'
};
// tomo las constantes creadas en constants/weathers.js

const getweatherIcon = weatherState => {
    const icon = icons[weatherState];
    const sizeIcon = "3x";

    if(icon) {
        return <WeatherIcons className="weatherIcon" name={icon} size={sizeIcon} />
    } else {
        return <WeatherIcons className="weatherIcon" name={'day-sunny'} size={sizeIcon} />
    }
}

const WeatherTemperature = ({ temperature, weatherState }) => (
    <div className="weatherTemperatureContent">
        {
            getweatherIcon(weatherState)
        }
        <span className="temperature">{`${temperature}`} </span>
        <span className="temperatureType">{`C°`}</span>
    </div>
);

WeatherTemperature.propTypes = {
    temperature: PropTypes.number.isRequired,
    weatherState: PropTypes.string.isRequired,
};
// en caso de que pasemos un tipo de dato incorrecto, nos mostrará un error por consola. Además, con 'isRequired' hacemos que dicho valor sea siempre requerido.

export default WeatherTemperature;