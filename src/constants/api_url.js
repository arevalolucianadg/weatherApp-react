const location = "Buenos Aires, ar";
const api_key = "d055715312752f717c7d967367248f01";
const url_base_weather = "http://api.openweathermap.org/data/2.5/weather";

export const api_weather = `${url_base_weather}?q=${location}&appid=${api_key}&units=metric`;