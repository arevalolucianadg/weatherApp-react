ESTRUCTURA
app.js/
    - WeatherLocation.js
        - Location.js
        - WeatherData.js
            - WeatherExtraInfo.js
            - WeatherTemperature.js

Sección 4
    - Creación de constantes con los nombres de estados.
    - PropTypes
        - Tipos de validaciones disponibles
            - PropTypes.array,
            - PropTypes.bool,
            - PropTypes.func,
            - PropTypes.number,
            - PropTypes.object,
            - PropTypes.string,
            - PropTypes.symbol,

            - PropTypes.node, // elemento React, aunque también se pueden incluir números, strings.
            - PropTypes.element, // Exclusivo para elementos React
            - PropTypes.instanceOf(Message),
            - PropTypes.oneOf(['News', 'Photos']),  // La propiedad está limitada a valores especificados, es decir, que puede ser cualquiera de los elementos que incluyamos en el array.
            - PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number,
                PropTypes.instanceOf(Message)
            ]),  // en este caso la propiedad puede ser de cualquiera de estos tipos.
            - PropTypes.arrayOf(PropTypes.number), // en primer término indicamos que es debe ser un array, y luego que tiene que ser de tipo númerico.
            - PropTypes.objectOf(PropTypes.number), // objeto de tipo numérico.
            - PropTypes.shape({
                color: PropTypes.string,
                fontSize: PropTypes.number,
            }),

Sección 5
    - Refactorización de carpetas.
    - Archivo styles.css en WeatherData/

Sección 6
    - En React hay dos tipos de componentes:
        - Class Components o Componentes de clase.
        - Functional Components o Componentes tipo función.
    - Tienen diferente sintaxis. 
    - Los componentes de clase permiten agregar más funcionalidades.

    - Evento Onclick y manejo de state

    - Manejo de vida de un componente
        - Constructor: en el constructor se establece el estado inicial de un componente, cuando llega el momento de renderizar, que es un momento posterior al que se construye el componente, se pasa por el método render() y con el estado inicial se hace la primera renderización. 
        - Para modificar el estado utilizo this.setState({ }). Con determinado evento, renderizo este nuevo estado.

Sección 7: Conectando con API
    - Registro en https://openweathermap.org. API key.
    - Fetch: es una instrucción nativa, que trae los datos del servidor para utilizarlos en nuestra aplicación. En caso, de que querer utilizarlo en navegadores más antiguos, se puede utilizar la librería Axios.
    - PROMISES: es un objeto que es utilizado para 'peticiones asíncronas', y representa un valor que puede estar disponible en el momento, en un futuro o nunca debido a un tipo de fallo. Pueden encontrarse en diferentes estados: pendiente, cumplida, rechazada. 

    - El método Fetch() toma un argumento obligatorio, la ruta de acceso al recurso que desea recuperar. Devuelve una Promise que resuelve en Response a esa petición, sea o no correcta. 

    - Para convertir temperatura de Kelvin a Celsius:
        - A través de la API, agregando el parámetro &units=metric al final de la url.
        - Con la librería convert-units.

    - Patrón de diseño SOLID. 
        . S: Single Responsability ( Única responsabilidad ). Cada una de las clases tiene que tener una única responsabilidad, para tener el menor nivel de acoplamiento. 

    - Los componentes visuales SOLO deben encargarse de la UI.
